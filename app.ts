let nombre:string = "Carlos";
let numero:number = 123;
let booleano:boolean = true;
let fecha:Date = new Date();
fecha =new Date('2019-08-23');

let cualquiera:any;

cualquiera = nombre;
cualquiera=numero;
cualquiera=booleano;
cualquiera = fecha;

//Template literales
let nom:string ="Carlos Adrian";
let ape:string ="Chango Lozano";
let edad:number =34

let texto:string =`Hola, 
${nom} 
${ape} 
(${34})`;

//Parametros obligatorios, por defecto, opcionales

function activar(quien:string,
                objeto:string="Angular",
                momento?:string){
let mensaje:string;
    if (momento)
    {
        mensaje=`${quien} estudia ${objeto} en la ${momento}`;
    }
    else
    {
        mensaje=`${quien} estudia ${objeto}`;
    }
}
activar("Carlos Adrian","Angular","tarde");


// Funciones de flecha
let miFuncion = function(a:any){
    return a;
}
let miFuncionF = (a:any) => a;

let miFuncion1 = function(a:number,b:number)
{
    return a+b;
}

let miFuncion1F = (a:number,b:number)=>a+b;

let miFuncion2= function(nombre:string){
    nombre=nombre.toUpperCase();
    return nombre;
}
let miFuncion2F = (nombre:string) => {
    nombre=nombre.toUpperCase();
    return nombre;
}

let hulk = {
    nombre:"Hulk",
    smash(){
        setTimeout(() => console.log(this.nombre + " smash!!"), 1500);
    }
}

hulk.smash();
