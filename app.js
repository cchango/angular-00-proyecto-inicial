"use strict";
let nombre = "Carlos";
let numero = 123;
let booleano = true;
let fecha = new Date();
fecha = new Date('2019-08-23');
let cualquiera;
cualquiera = nombre;
cualquiera = numero;
cualquiera = booleano;
cualquiera = fecha;
//Template literales
let nom = "Carlos Adrian";
let ape = "Chango Lozano";
let edad = 34;
let texto = `Hola, 
${nom} 
${ape} 
(${34})`;
//Parametros obligatorios, por defecto, opcionales
function activar(quien, objeto = "Angular", momento) {
    let mensaje;
    if (momento) {
        mensaje = `${quien} estudia ${objeto} en la ${momento}`;
    }
    else {
        mensaje = `${quien} estudia ${objeto}`;
    }
}
activar("Carlos Adrian", "Angular", "tarde");
// Funciones de flecha
let miFuncion = function (a) {
    return a;
};
let miFuncionF = (a) => a;
let miFuncion1 = function (a, b) {
    return a + b;
};
let miFuncion1F = (a, b) => a + b;
let miFuncion2 = function (nombre) {
    nombre = nombre.toUpperCase();
    return nombre;
};
let miFuncion2F = (nombre) => {
    nombre = nombre.toUpperCase();
    return nombre;
};
let hulk = {
    nombre: "Hulk",
    smash() {
        setTimeout(() => console.log(this.nombre + " smash!!"), 1500);
    }
};
hulk.smash();
