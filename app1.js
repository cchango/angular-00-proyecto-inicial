"use strict";
//Destructuracion de objectos y arreglos
//Objecto
let mensaje = {
    nombre: "Carlos",
    clave: "guambra vago",
    poder: "chocolate"
};
//  let nombre = mensaje.nombre;
//  let clave = mensaje.clave;
//  let poder = mensaje.poder;
let { nombre, clave, poder } = mensaje;
//Arreglos
let avengers = ["Thor", "Steve", "Tony"];
let [dato1, dato2, dato3] = avengers;
//console.log(dato1,dato2,dato3);
// Promesa ES6
let miPrimeraPromesa = new Promise(function (resolve, reject) {
    console.log("Iniciando la promesa");
    setTimeout(() => {
        //Si termina Bien
        //resolve();
        //Si termina mal
        reject();
    }, 1500);
});
console.log("Paso 1");
miPrimeraPromesa.then(function () {
    console.log("Esta promesa se ejecuto bien");
}, function () {
    console.error("Esta promesa se ejecuto con error");
});
console.log("Paso 2");
