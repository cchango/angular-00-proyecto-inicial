//Interfaces en typescript
interface Xmen{
    nombre:string,
    poder:string
}

function enviarMision(xmen:Xmen){
console.log("Enviar a: "+ xmen.nombre);
}

function enviarCuartel(xmen:Xmen){
    console.log("Enviar al cuartel: "+ xmen.nombre);
    }

let wolverine:Xmen = {
 nombre: "Wolverine",
 poder:"Regeneracion"
}

//Definicion de una clase

class Alumno{
     nombres:string;
     apellidos:string;
     edad:number;
     telefono:number;
     constructor(nombres:string, apellidos:string, edad:number ,telefono:number ){
        this.nombres=nombres;
        this.apellidos=apellidos;
        this.edad=edad;
        this.telefono=telefono;
     }
}

let estudiante:Alumno = new Alumno("Carlos Adrian", "Chango Lozano", 33, 256895);

console.log(estudiante);