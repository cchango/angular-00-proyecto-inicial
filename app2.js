"use strict";
function enviarMision(xmen) {
    console.log("Enviar a: " + xmen.nombre);
}
function enviarCuartel(xmen) {
    console.log("Enviar al cuartel: " + xmen.nombre);
}
let wolverine = {
    nombre: "Wolverine",
    poder: "Regeneracion"
};
//Definicion de una clase
class Alumno {
    constructor(nombres, apellidos, edad, telefono) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.edad = edad;
        this.telefono = telefono;
    }
}
let estudiante = new Alumno("Carlos Adrian", "Chango Lozano", 33, 256895);
console.log(estudiante);
